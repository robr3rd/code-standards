# Overview
- Line length
	- 80 columns or less is wonderful
	- 120 columns or less is the maximum
- Naming conventions
	- classes/interfaces/traits = `StudlyCaps`
	- functions/methods = `camelCase`
	- variables/properties = `snake_case`
	- constants = `SNAKE_CASE_CAPS`
	- files/folders:
		- PHP classes/namespaces = `StudlyCaps`
		- everything else = `kebab-case`
- Tabs vs. Spaces
	- Use tabs for indentation because of their accessibility and semantic meaning
	- Use spaces for alignment
	- Reason:
		- Short answer: Saying "versus" is ridiculous since they each have different semantic meanings/purposes.
- Brace style
	- Use the 1TBS ("One True Brace Style")
		- `class Foo {`
		- `function fooBar() {`
		- `if (condition) {`
	- In short:
		- Always use braces (even when they could be optional like in if statements)
		- Opening braces go on the same line as the thing they are declaring
- Whitespace
	- 1 line separates for readability
	- 2 lines separate sections (ex: between methods/functions)
	- 3 lines separate concepts (ex: logical groups of properties or methods)
- Docblocks
	- These exist for an editor to make a developer's life easier
	- Modern PHP has enough of a typing system that editors can just read the code.
	- Only declare a type via docblock when your PHP version does not support typing via code.
- Typing
	- Fully utilize as much native typing as is supported by your version of PHP.
		- `function myFoo(int $foo, ?Iterable $bar, null|bool|string $baz): ?object {`
- Strict vs. Loose comparisons
	- Prefer strict over loose
	- Good: `if ($foo === 'foo') {`
	- Bad: `if ($foo == 'foo') {`
- Avoid "Yoda conditions"
	- Good: `if ($foo !== 'foo') {`
	- Bad: `if (!($foo === 'foo')) {`
- Use trailing commas on multi-line arrays and function parameters
	- This reduces Git conflicts by producing cleaner diffs
- Concatenation vs. Interpolation
	- Prefer interpolation as it is less mentally taxing to parse, but use concatenation where it makes more sense.
	- Bad: `$foo = 'First: ' . $user→firstname . ' / Last: ' . $user→lastname;`
	- Good: `$foo = "First name: {$user->firstname} / Last: {$user->lastname}";`
- Avoid 'if' conditions when possible
	- Code is usually--but not always--better off using DI, IoC, polymorphism, etc., as these patterns are typically more flexible/extensible/etc.
	- Also, the null-coalescing, Elvis, spaceship, and null-safe operators are your friends.  In order these are:
		- `echo $foo ?? 'default value';`
		- `echo $foo ?: 'default value';`
		- `echo 'Change direction: ' . $foo <=> $bar;`
		- `echo $foo?->property;`
		- (all together) `echo $foo?→myMethod() ?? 'default value';`
	- With that being said, if you really do have just one path and have reason to believe that won't change for a long time then by all means use an 'if'. 



## PHP CodeSniffer (PHPCS) file
Here is a copy of it for convenience while you're here, but it is encouraged to use the actual file in this repository instead

```xml
<?xml version="1.0"?>
<ruleset name="Robr3rd">
	<description>
		Robr3rd's personal coding standard

		Essentially, the standard is PSR-1 + "custom" PSR-12 with "custom" defined as follows:
		- Indent with tabs instead of spaces (note: spaces are for alignment, ex: multi-line method chaining).
		- Brace style follows the 1TBS ("One True Brace Style") so braces go on the same line rather than a new line.
	</description>

	<rule ref="PSR1" />

	<rule ref="PSR2">
		<exclude name="Generic.WhiteSpace.DisallowTabIndent" />
		<exclude name="PSR2.Classes.ClassDeclaration.OpenBraceNewLine" />
		<exclude name="Squiz.Functions.MultiLineFunctionDeclaration.BraceOnSameLine" />
	</rule>

	<!-- Whitespace and Indentation -->
	<rule ref="Generic.WhiteSpace.DisallowSpaceIndent" />
	<rule ref="Generic.WhiteSpace.ScopeIndent">
		<properties>
			<property name="indent" value="4" />
			<property name="tabIndent" value="true" />
		</properties>
	</rule>

	<!-- Lines -->
	<rule ref="Generic.Files.LineEndings">
		<properties>
			<property name="eolChar" value="\n"/>
		</properties>
	</rule>
	<rule ref="Generic.Files.LineLength">
		<properties>
			<property name="lineLimit" value="120" />
			<property name="absoluteLineLimit" value="0" />
		</properties>
	</rule>

	<!-- Braces -->
	<rule ref="Generic.Classes.OpeningBraceSameLine" />
	<rule ref="Generic.Functions.OpeningFunctionBraceKernighanRitchie" />

	<!-- Comments -->
	<rule ref="Squiz.Commenting.FunctionCommentThrowTag" />
</ruleset>
```



## Example code
```php
<?php

namespace Pepperjam\SomeNamespace;

use VendorA\PackageA\FirstTrait;
use VendorA\PackageB\SecondTrait;

use VendorB\CoolPackage;

use VendorC\BestPackage;

use VendorD\InterestingInterface;

/**
 * Purpose of class (unless obvious to a mid-tier new hire seeing this code for the first time)
 */
class SomeClass extends CoolPackage implements InterestingInterface {
	use FirstTrait;
	use SecondTrait;

	const SOME_CONSTANT_HERE = 'value';  // no docblock because constants always have values (and therefore a type)
	
	/**
	 * @var int 
	 */
	private $foo_bar_baz = 100;


	/**
	 * Purpose of function (unless obvious to a mid-tier new hire seeing this code for the first time)
	 */
	public function simpleFunction(): string {
		$foo = new BestPackage();

		$x =
			$foo->one('a', 'b')
			    ->two('c', 'd', 'e')
			    ->three('fg')
			    ->four();

		$y = (new BestPackage())->a()->b()->c();

		return 'thing';
	}


	/**
	 * Purpose of function (unless obvious to a mid-tier new hire seeing this code for the first time)
	 */
	public function wrapperFunction(): SomeClass {
		self::simpleDemo(false, $this->foo_bar_baz);  // variables/properties (snake_case)
		self::simpleDemo(false, $this->simpleFunction());  // functions/methods (camelCase)
		self::simpleDemo(false, self::SOME_CONSTANT_HERE);  // constants (SNAKE_CASE_CAPS)

		return $this;
	}


	/**
	 * Purpose of function (unless obvious to a mid-tier new hire seeing this code for the first time)
	 */
	final public static function simpleDemo(bool $foo, int $bar = 100): array {
		if ($foo === false) {  // identicality vs equality when possible
			echo 'False!';
			return [];  // use early-returns when possible (reduces code complexity further down)
		}

		echo 'Some text!' .  // indent with tabs; align with spaces
		     self::SOME_CONSTANT_HERE .
		     ($foo === true ? 'True!' : 'False!') .  // ternaries go in parentheses for sanity
		     'Some more text!';

		if ($bar === 100) {  // identicality vs equality when possible
			echo '100%!';
		} else {  // all 'if's have braces, including single-statement ones
			echo 'Not 100%';
		}

		$arr = [  // avoid useless arrays (like this example which should be in the 'return')
			'use',
			'short',
			'array',
			'syntax',  // use trailing commas
		];
		
		return $arr;
	}


	/**
	 * Purpose of function (unless obvious to a mid-tier new hire seeing this code for the first time)
	 */
	public function explainConcatenation(
		$arg1,
		$arg2
	): string {
		return "{$arg1} and {$arg2} concatenated make {$arg1}{$arg2}";
	}


	/**
	 * This docblock is for PHP 5.6 since typed parameters are unsupported
	 *
	 * @param integer $foo    Foo does x
	 * @param array   $barium Barium does y
	 * @return integer
	 */
	public function foo($foo, $barium) {
		return 0;
	}
}
```
